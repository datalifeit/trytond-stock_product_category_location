# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['ShipmentOut', 'ShipmentOutReturn', 'ShipmentIn']


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    def _get_inventory_move(self, move):
        res = super()._get_inventory_move(move)
        if not res or hasattr(res, 'unit_load'):
            return res
        for c in res.product.categories:
            _from_location = c.get_default_location(
                **self._get_default_location_params())
            if _from_location:
                res.from_location = _from_location
                break
        return res

    def _get_default_location_params(self):
        return {'warehouse': self.warehouse}


class ShipmentOutReturn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    def _get_inventory_move(self, incoming_move):
        res = super()._get_inventory_move(incoming_move)
        if not res:
            return res
        for c in res.product.categories:
            _to_location = c.get_default_location(
                **self._get_default_location_params(incoming_move))
            if _to_location:
                res.to_location = _to_location
                break
        return res

    @classmethod
    def _get_default_location_params(cls, incoming_move):
        return {'warehouse': incoming_move.shipment.warehouse}


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    def _get_inventory_move(self, incoming_move):
        res = super()._get_inventory_move(incoming_move)
        if not res:
            return res
        for c in res.product.categories:
            _to_location = c.get_default_location(
                **self._get_default_location_params(incoming_move))
            if _to_location:
                res.to_location = _to_location
                break
        return res

    @classmethod
    def _get_default_location_params(cls, incoming_move):
        return {'warehouse': incoming_move.shipment.warehouse}
