==================================================
Check default and allowed location for categories
==================================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)

Install Module::

    >>> config = activate_modules('stock_product_category_location')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> input_loc, = Location.find([('code', '=', 'IN')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])

Check default for warehouse field while allow new category to a location::

    >>> cat_loc = storage_loc.categories.new()
    >>> cat_loc.warehouse == storage_loc.parent
    True
    >>> Category = Model.get('product.category')
    >>> cat = Category(name='test cat')
    >>> cat.save()
    >>> cat_loc.category = cat
    >>> storage_loc.save()

Check sequence compute in locations form::

    >>> storage_loc.reload()
    >>> storage_loc.categories[0].sequence
    1
    >>> cat_loc = storage_loc.categories.new()
    >>> cat = Category(name='test cat2')
    >>> cat.save()
    >>> cat_loc.category = cat
    >>> storage_loc.save()
    >>> storage_loc.reload()
    >>> storage_loc.categories[0].sequence
    1
    >>> storage_loc.categories[1].sequence
    1
    >>> test_storage = Location(name='Test storage',
    ...     type='storage', parent=storage_loc)
    >>> test_storage.save()
    >>> test_storage.reload()
    >>> cat_loc = test_storage.categories.new()
    >>> cat_loc.category = cat
    >>> test_storage.save()
    >>> test_storage.reload()
    >>> test_storage.categories[0].sequence
    2
    >>> test_storage.categories[0].warehouse == warehouse_loc
    True

Check default location in Shipment out::

    >>> template.categories.append(cat)
    >>> template.save()
    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company
    >>> StockMove = Model.get('stock.move')
    >>> shipment_out.outgoing_moves.append(StockMove())
    >>> move = shipment_out.outgoing_moves[0]
    >>> move.product = product
    >>> move.uom =unit
    >>> move.quantity = 1
    >>> move.from_location = output_loc
    >>> move.to_location = customer_loc
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> shipment_out.save()
    >>> shipment_out.click('wait')
    >>> len(shipment_out.inventory_moves)
    1
    >>> inv_move = shipment_out.inventory_moves[0]
    >>> inv_move.from_location == storage_loc
    True
    >>> shipment_out.click('draft')
    >>> inv_move.delete()
    >>> storage_loc.categories[1].sequence = 3
    >>> storage_loc.save()
    >>> shipment_out.click('wait')
    >>> inv_move = shipment_out.inventory_moves[0]
    >>> inv_move.from_location == test_storage
    True

Check default location in Shipment in::

    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment_in = ShipmentIn()
    >>> shipment_in.planned_date = today
    >>> shipment_in.supplier = customer
    >>> shipment_in.warehouse = warehouse_loc
    >>> shipment_in.company = company
    >>> StockMove = Model.get('stock.move')
    >>> move = shipment_in.incoming_moves.new()
    >>> move.product = product
    >>> move.uom =unit
    >>> move.quantity = 1
    >>> move.from_location = supplier_loc
    >>> move.to_location = input_loc
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> shipment_in.save()
    >>> shipment_in.click('receive')
    >>> len(shipment_in.inventory_moves)
    1
    >>> shipment_in.inventory_moves[0].to_location == test_storage
    True
