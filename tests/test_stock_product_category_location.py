# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.exceptions import UserError
import doctest
from trytond.tests.test_tryton import doctest_teardown, doctest_checker


class StockProductCategoryLocationTestCase(ModuleTestCase):
    """Test Stock Product Category Location module"""
    module = 'stock_product_category_location'

    @with_transaction()
    def test_add_category_default_location(self):
        """Add the category default location for a warehouse"""
        pool = Pool()
        Location = pool.get('stock.location')
        Category = pool.get('product.category')
        CatLoc = pool.get('stock.product.category.location')

        storage, = Location.search([('code', '=', 'STO')])
        new_cat, = Category.create([{'name': 'CAT1'}])
        cat_loc_rec = {
            'category': new_cat.id,
            'warehouse': storage.parent.id,
            'location': storage.id
        }
        CatLoc.create([cat_loc_rec])

    @with_transaction()
    def test_category_location_unique(self):
        """Check unique error"""
        pool = Pool()
        Location = pool.get('stock.location')
        Category = pool.get('product.category')
        CatLoc = pool.get('stock.product.category.location')

        storage, = Location.search([('code', '=', 'STO')])
        new_cat, = Category.create([{'name': 'CAT1'}])
        cat_loc_rec = {
            'category': new_cat.id,
            'warehouse': storage.parent.id,
            'location': storage.id
        }
        CatLoc.create([cat_loc_rec])

        with self.assertRaises(UserError) as cm:
            CatLoc.create([cat_loc_rec])
        self.assertEqual(
            cm.exception.message, 'Category and location must be unique.')

    @with_transaction()
    def test_add_parent_category_default_location(self):
        """Add the category default location for a warehouse"""
        pool = Pool()
        Location = pool.get('stock.location')
        Category = pool.get('product.category')
        CatLoc = pool.get('stock.product.category.location')

        storage, = Location.search([('code', '=', 'STO')])
        storage_new, = Location.create([{
            'code': 'STO2',
            'name': 'Storage 2',
            'type': 'storage',
            'parent': storage.id
        }])
        new_cat, = Category.create([{'name': 'CAT1'}])
        child_cat, = Category.create([{'name': 'CAT2', 'parent': new_cat.id}])
        cat_loc_rec = {
            'category': new_cat.id,
            'warehouse': storage.warehouse.id,
            'location': storage.id
        }
        CatLoc.create([cat_loc_rec])

        self.assertEqual(child_cat.get_default_location(), storage)

        CatLoc.create([{
            'category': child_cat.id,
            'warehouse': storage_new.warehouse.id,
            'location': storage_new.id
        }])

        self.assertEqual(child_cat.get_default_location(), storage_new)

    @with_transaction()
    def test_warehouse_isolation(self):
        """Add the category default location for a warehouse"""
        pool = Pool()
        Location = pool.get('stock.location')
        Category = pool.get('product.category')
        CatLoc = pool.get('stock.product.category.location')

        storage, = Location.search([('code', '=', 'STO')])
        storage_new, = Location.create([{
            'code': 'STO2',
            'name': 'Storage 2',
            'type': 'storage',
            'parent': storage.id
        }])
        new_cat, = Category.create([{'name': 'CAT1'}])
        cat_loc_rec = {
            'category': new_cat.id,
            'warehouse': storage.warehouse.id,
            'location': storage.id
        }
        CatLoc.create([cat_loc_rec])

        self.assertEqual(new_cat.get_default_location(), storage)

        # create new warehouse
        new_wh, = Location.copy([storage.warehouse])

        self.assertEqual(new_cat.get_default_location(warehouse=new_wh), None)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        StockProductCategoryLocationTestCase))
    suite.addTests(doctest.DocFileSuite(
        'scenario_stock_product_category_location.rst',
        tearDown=doctest_teardown, encoding='utf-8',
        checker=doctest_checker,
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
